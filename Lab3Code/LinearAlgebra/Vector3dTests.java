//Mikito Takata
//2036737

package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests 
{
    @Test
    public void testCreationAndGetMethods()
    {
        Vector3d v1 = new Vector3d(1, 2, 3);
        assertEquals(1, v1.getX());
        assertEquals(2, v1.getY());
        assertEquals(3, v1.getZ());
    }
    

    @Test
    public void testMagnitute()
    {
        Vector3d v1 = new Vector3d(1, 2, 3);
        double expected = Math.sqrt(14);

        assertEquals(expected, v1.magnitude());
    }

    @Test
    public void testDotProduct()
    {
        Vector3d v1 = new Vector3d(1, 2, 3);
        Vector3d v2 = new Vector3d(4, 5, 6);

        double expected = 4+10+18;

        assertEquals(expected, v1.dotProduct(v2));
    }

    @Test 
    public void testAdd()
    {
        Vector3d v1 = new Vector3d(1, 2, 3);
        Vector3d v2 = new Vector3d(4, 5, 6);

        Vector3d expected = new Vector3d(5, 7, 9);
        Vector3d actual = v1.add(v2);
        assertEquals(expected.getX(), actual.getX());
        assertEquals(expected.getY(), actual.getY());
        assertEquals(expected.getZ(), actual.getZ());
    }
}
